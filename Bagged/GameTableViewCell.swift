//
//  GameTableViewCell.swift
//  Bagged
//
//  Created by Uzziel Luna on 11/11/17.
//  Copyright © 2017 Game Points. All rights reserved.
//

import UIKit

class GameTableViewCell: UITableViewCell {
    //MARK: Properties
    @IBOutlet weak var gameNameLabel: UILabel!
    @IBOutlet weak var timeAndDateLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var photoImageView: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
