//
//  ViewController.swift
//  Bagged
//
//  Created by Uzziel Luna on 11/2/17.
//  Copyright © 2017 Game Points. All rights reserved.
//

import UIKit
import os.log

class GameViewController: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    //MARK: Properties
    @IBOutlet weak var dateAndTimeTextField: UITextField!
    @IBOutlet weak var gameTypeTextfield: UITextField!
    @IBOutlet weak var numberofPointsTextField: UITextField!
    @IBOutlet weak var weightLbsTextField: UITextField!
    @IBOutlet weak var weightOzTextField: UITextField!
    @IBOutlet weak var locationTextField: UITextField!
    @IBOutlet weak var methodUsedTextField: UITextField!
    @IBOutlet weak var weatherConditionsTextField: UITextField!
    @IBOutlet weak var notesTextField: UITextField!
    
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var saveButton: UIBarButtonItem!
    
    //MARK: Variable
    /*
     This value is either passed by `MealTableViewController` in `prepare(for:sender:)`
     or constructed as part of adding a new meal.
     */
    var game: Game?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Handle the text field’s user input through delegate callbacks.
        dateAndTimeTextField.delegate = self
        gameTypeTextfield.delegate = self
        numberofPointsTextField.delegate = self
        weightLbsTextField.delegate = self
        weightOzTextField.delegate = self
        locationTextField.delegate = self
        methodUsedTextField.delegate = self
        weatherConditionsTextField.delegate = self
        notesTextField.delegate = self
        
        // Set up views if editing an existing Meal.
        if let game = game {
            navigationItem.title = game.gameType
            dateAndTimeTextField.text = game.dateAndTime
            photoImageView.image = game.photo
            gameTypeTextfield.text = game.gameType
            //Int(numberofPointsTextField.text) = game.numberofPoints
            //weightLbsTextField.text = game.
            //weightOzTextField.text = game.
            locationTextField.text = game.location
            methodUsedTextField.text = game.methodUsed
            weatherConditionsTextField.text = game.weatherCondition
            notesTextField.text = game.notes            
        }
        
        //These are calls that allow the keyboard to move up or down
        //There is a bug in this because when the user chooses a new input it does not slide down
        //will have to look into this further.  Leaving as is right now and will re-look at this code
        //again.
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: Notification.Name.UIKeyboardWillHide, object: nil)
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: Notification.Name.UIKeyboardWillChangeFrame, object: nil)
    }
    
    //MARK: Keyboard
        @objc func adjustForKeyboard(notification: Notification) {
            let userInfo = notification.userInfo!
            
            let keyboardScreenEndFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
            let keyboardViewEndFrame = view.convert(keyboardScreenEndFrame, from: view.window)
            
            if notification.name == Notification.Name.UIKeyboardWillHide {
                scrollView.contentInset = UIEdgeInsets.zero
            }
            if notification.name == Notification.Name.UITextFieldTextDidBeginEditing {
                scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardViewEndFrame.height + 80, right: 0)
            } else {
                scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardViewEndFrame.height + 80, right: 0)
            }
            
            scrollView.scrollIndicatorInsets = scrollView.contentInset
        }

    
   //MARK: UITextFieldDelegate
    func textFieldDidBeginEditing(_ textField: UITextField){
        //true
    }
   func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        // Hide the keyboard.
        textField.resignFirstResponder()
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        //mealNameLabel.text = textField.text
    }
    
    //MARK: UIImagePickerControllerDelegate
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        // Dismiss the picker if the user canceled.
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        // The info dictionary may contain multiple representations of the image. You want to use the original.
        guard let selectedImage = info[UIImagePickerControllerOriginalImage] as? UIImage else {
            fatalError("Expected a dictionary containing an image, but was provided the following: \(info)")
        }
        
        // Set photoImageView to display the selected image.
        photoImageView.image = selectedImage
        
        // Dismiss the picker.
        dismiss(animated: true, completion: nil)
    }
    
    //MARK: Navigation
    @IBAction func cancel(_ sender: UIBarButtonItem) {
        // Depending on style of presentation (modal or push presentation), this view controller needs to be dismissed in two different ways.
        let isPresentingInAddMealMode = presentingViewController is UINavigationController
        
        if isPresentingInAddMealMode {
            dismiss(animated: true, completion: nil)
        }
        else if let owningNavigationController = navigationController{
            owningNavigationController.popViewController(animated: true)
        }
        else {
            fatalError("The GameViewController is not inside a navigation controller.")
        }
    }
    
    // This method lets you configure a view controller before it's presented.
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        // Configure the destination view controller only when the save button is pressed.
        guard let button = sender as? UIBarButtonItem, button === saveButton else {
            os_log("The save button was not pressed, cancelling", log: OSLog.default, type: .debug)
            return
        }
        let dateAndTime = dateAndTimeTextField.text ?? ""
        let photo = photoImageView.image
        let gameType = gameTypeTextfield.text ?? ""
        let numberofPoints = Int(numberofPointsTextField.text!) ?? 0
        let weightLbs = Int(weightLbsTextField.text!) ?? 0
        let weightOz = Int(weightOzTextField.text!) ?? 0
        let location = locationTextField.text ?? ""
        let methodUsed = methodUsedTextField.text ?? ""
        let weatherCondition = weatherConditionsTextField.text ?? ""
        let notes = notesTextField.text ?? ""
        
        //Add the restrictions to what needs to inputted and what is useful for input
        //In other words do not save if there are fields that are not filled out
        game = Game(dateAndTime: dateAndTime, photo: photo, gameType: gameType, numberofPoints: numberofPoints, weightLbs: weightLbs, weightOz: weightOz, location: location, methodUsed: methodUsed, weatherCondition: weatherCondition, notes: notes)
        
    }
    
    //MARK: Action
    @IBAction func takePhoto(_ sender: UIButton) {
        // Hide the keyboard.
        //dateAndTimeTextField.resignFirstResponder()
        
        // UIImagePickerController is a view controller that lets a user pick media from their photo library.
        let imagePickerController = UIImagePickerController()
        
        // Only allow photos to be picked, not taken.
        imagePickerController.sourceType = .photoLibrary
        
        // Make sure ViewController is notified when the user picks an image.
        imagePickerController.delegate = self
        present(imagePickerController, animated: true, completion: nil)
    }


}

