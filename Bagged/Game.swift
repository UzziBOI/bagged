//
//  Game.swift
//  Bagged
//
//  Created by Uzziel Luna on 11/8/17.
//  Copyright © 2017 Game Points. All rights reserved.
//

import UIKit
import os.log

class Game: NSObject, NSCoding {
    
    
    //MARK: Properties
    var dateAndTime: String
    var photo: UIImage?
    var gameType: String
    //var sex: UISegmentedControl
    var numberofPoints: Int
    var weightLbs: Int
    var weightOz: Int
    var location: String
    var methodUsed: String
    var weatherCondition: String
    var notes: String
    
    
    //MARK: Archiving Paths
    static let DocumentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    static let ArchiveURL = DocumentsDirectory.appendingPathComponent("allGame")

    
    //MARK: Types
    struct PropertyKey {
        static let dateAndTime = "dateAndTime"
        static let photo = "photo"
        static let gameType = "gameType"
        static let numberofPoints = "numberofPoints"
        static let weightLbs = "weightLbs"
        static let weightOz = "weightOz"
        static let location = "location"
        static let methodUsed = "methodUsed"
        static let weatherCondition = "weatherCodition"
        static let notes = "notes"
    }
    
    //MARK: Initialize stored properties.
    init? (dateAndTime: String, photo: UIImage?, gameType: String, /*sex: UISegmentedControl,*/ numberofPoints: Int, weightLbs: Int, weightOz: Int, location: String, methodUsed: String, weatherCondition: String, notes: String){
        // Initialization should fail if there is no name or if the rating is negative.
        if gameType.isEmpty || dateAndTime.isEmpty || location.isEmpty  {
            return nil
        }
        
        //initialize stored properties
        self.dateAndTime = dateAndTime
        self.photo = photo
        self.gameType = gameType
        //self.sex = sex
        self.numberofPoints = numberofPoints
        self.weightLbs = weightLbs
        self.weightOz = weightOz
        self.location = location
        self.methodUsed = methodUsed
        self.weatherCondition = weatherCondition
        self.notes = notes
    }
    //MARK: NSCoding
    func encode(with aCoder: NSCoder) {
        aCoder.encode(dateAndTime, forKey: PropertyKey.dateAndTime)
        aCoder.encode(photo, forKey: PropertyKey.photo)
        aCoder.encode(gameType, forKey: PropertyKey.gameType)
        aCoder.encode(numberofPoints, forKey: PropertyKey.numberofPoints)
        aCoder.encode(weightLbs, forKey: PropertyKey.weightLbs)
        aCoder.encode(weightOz, forKey: PropertyKey.weightOz)
        aCoder.encode(location, forKey: PropertyKey.location)
        aCoder.encode(methodUsed, forKey: PropertyKey.methodUsed)
        aCoder.encode(weatherCondition, forKey: PropertyKey.weatherCondition)
        aCoder.encode(notes, forKey: PropertyKey.notes)
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        
        // The name is required. If we cannot decode a name string, the initializer should fail.
        guard let gameType = aDecoder.decodeObject(forKey: PropertyKey.gameType) as? String else {
            os_log("Unable to decode the name for a Meal object.", log: OSLog.default, type: .debug)
            return nil
        }
        guard let dateAndTime = aDecoder.decodeObject(forKey: PropertyKey.dateAndTime) as? String else {
            os_log("Unable to decode the name for a Meal object.", log: OSLog.default, type: .debug)
            return nil
        }
        
        // Because photo is an optional property of Meal, just use conditional cast.
        let photo = aDecoder.decodeObject(forKey: PropertyKey.photo) as? UIImage
        
        
        let numberofPoints = aDecoder.decodeInteger(forKey: PropertyKey.numberofPoints)
        let weightLbs = aDecoder.decodeInteger(forKey: PropertyKey.weightLbs)
        let weightOz = aDecoder.decodeInteger(forKey: PropertyKey.weightOz)
        let location = aDecoder.decodeObject(forKey: PropertyKey.location)
        let methodUsed = aDecoder.decodeObject(forKey: PropertyKey.methodUsed)
        let weatherCondition = aDecoder.decodeObject(forKey: PropertyKey.weatherCondition)
        let notes = aDecoder.decodeObject(forKey: PropertyKey.notes)

        
        
        
        // Must call designated initializer.
        
        self.init(dateAndTime: dateAndTime, photo: photo, gameType: gameType, /*sex: UISegmentedControl,*/ numberofPoints: numberofPoints, weightLbs: weightLbs, weightOz: weightOz, location: location as! String, methodUsed: methodUsed as! String, weatherCondition: weatherCondition as! String, notes: notes as! String)

        
    }
}
