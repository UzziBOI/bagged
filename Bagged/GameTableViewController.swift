//
//  GameTableViewController.swift
//  Bagged
//
//  Created by Uzziel Luna on 11/11/17.
//  Copyright © 2017 Game Points. All rights reserved.
//

import UIKit
import os.log

class GameTableViewController: UITableViewController {
    
    //MARK: Properties
    
    var allGame = [Game]()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Use the edit button item provided by the table view controller.
        navigationItem.leftBarButtonItem = editButtonItem
        
        // Load any saved meals, otherwise load sample data.
        if let savedAllGame = loadAllGame() {
            allGame += savedAllGame
        }else {
            // Load the sample data.
            loadSampleGame()
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return allGame.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "GameTableViewCell"
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? GameTableViewCell  else {
            fatalError("The dequeued cell is not an instance of MealTableViewCell.")
        }

        // Fetches the appropriate meal for the data source layout.
        let game = allGame[indexPath.row]
        
        cell.gameNameLabel.text = game.gameType
        cell.timeAndDateLabel.text = game.dateAndTime
        cell.locationLabel.text = game.location
        cell.photoImageView.image = game.photo
        
        return cell
    }
    

    
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
 

    
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            allGame.remove(at: indexPath.row)
            saveAllGame()
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        super.prepare(for: segue, sender: sender)
        switch(segue.identifier ?? "") {
        case "AddGame":
            os_log("Adding new Game.", log: OSLog.default, type: .debug)
        
        case "ShowDetail":
            guard let gameDetailViewController = segue.destination as? GameViewController else {
                fatalError("Unexpected destination: \(segue.destination)")
            }
            
            guard let selectedGameCell = sender as? GameTableViewCell else {
                fatalError("Unexpected sender: \(String(describing: sender))")
            }
            
            guard let indexPath = tableView.indexPath(for: selectedGameCell) else {
                fatalError("The selected cell is not being displayed by the table")
            }
            
            let selectedGame = allGame[indexPath.row]
            gameDetailViewController.game = selectedGame
            
        default:
            fatalError("Unexpected Segue Identifier; \(String(describing: segue.identifier))")
        }
    }
    
    
    //MARK: Private Methods
    
    private func loadSampleGame() {
        let photo1 = UIImage(named: "whitetail")
        //let sex1 = UISegmentedControl
        
        guard let game1 = Game(dateAndTime: "November 11, 2017" , photo: photo1, gameType: "White-Tail Deer", /*sex: sex1,*/ numberofPoints: 5, weightLbs: 250, weightOz: 1, location: "Lubbock, Texas", methodUsed: "30-06", weatherCondition: "cloudy", notes: "") else {
            fatalError("Unable to instantiate game1")
        }
        allGame += [game1]
    }
    private func saveAllGame() {
        let isSuccessfulSave = NSKeyedArchiver.archiveRootObject(allGame, toFile: Game.ArchiveURL.path)
        if isSuccessfulSave {
            os_log("Meals successfully saved.", log: OSLog.default, type: .debug)
        } else {
            os_log("Failed to save meals...", log: OSLog.default, type: .error)
        }
    }
    
    //MARK: Actions
    @IBAction func unwindToMealList(sender: UIStoryboardSegue) {
        if let sourceViewController = sender.source as? GameViewController, let game = sourceViewController.game {
            if let selectedIndexPath = tableView.indexPathForSelectedRow {
                // Update an existing meal.
                allGame[selectedIndexPath.row] = game
                tableView.reloadRows(at: [selectedIndexPath], with: .none)
            } else {
            
            // Add a new meal.
            let newIndexPath = IndexPath(row: allGame.count, section: 0)
            
            allGame.append(game)
            tableView.insertRows(at: [newIndexPath], with: .automatic)
            }
            // Save allGame.
            saveAllGame()
        }
    }
    
    private func loadAllGame() -> [Game]? {
        return NSKeyedUnarchiver.unarchiveObject(withFile: Game.ArchiveURL.path) as? [Game]
        
    }
}
